/* we need to require all the pages we create on "routes.js" file so that a link can be 
  established. If we do not need this the  request flow will be interrupted */

  module.exports =  (app,db)=>{
    require('./routes/auth/__AUTHcred.js')(app, db);

}

