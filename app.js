

var createError = require('http-errors');
var express = require('express');
const dateformat = require('dateformat');
const moment = require('moment');


var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongodb = require('mongodb');//injecting database onto backend.
const assert = require('assert');
var cors = require('cors')
const cron = require("node-cron");
var ObjectId = require('mongodb').ObjectID;

const MYSTUDENT = 'PStudent';

// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');
const config = require('./config');

const PORT = 4000;

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', indexRouter);
// app.use('/users', usersRouter);
var originsWhitelist = [
  'http://localhost:8100' // this is my front-end url for development
]
var corsOptions = {
  origin: function (origin, callback) {
      var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
      callback(null, isWhitelisted);
  },
  credentials: true
}
//cron job
cron.schedule("0 12 * * *", function() {
  
  db.collection(MYSTUDENT).find({}).toArray().then(data => {
    if(data.length>=0){
      var tdate=new Date();
      var formatedDate =dateformat(tdate, 'dd-mm-yyyy');
      console.log(formatedDate,"date here")
      for(let mystudent of data){
        console.log(mystudent,"agane parayavo")
        if(mystudent.endDate==formatedDate){
          if(mystudent.paymentmode=="1month"){
           var amountUp= mystudent.registrationFee+(mystudent.pending)
           var new_date = moment(tdate).add(1, 'month').format("DD-MM-YYYY");
           console.log("done...")

          }
          if(mystudent.paymentmode=="3month"){
            var amountUp=  mystudent.registrationFee+(mystudent.pending);
            var new_date = moment(tdate).add(3, 'month').format("DD-MM-YYYY");

          }
          if(mystudent.paymentmode=="6month"){
            var amountUp=  mystudent.registrationFee+(mystudent.pending);
            var new_date = moment(tdate).add(6, 'month').format("DD-MM-YYYY");

          }
          if(mystudent.paymentmode=="yearly"){
            var amountUp=  mystudent.registrationFee+(mystudent.pending);
            var new_date = moment(tdate).add(1, 'year').format("DD-MM-YYYY");

          }
          
          // var data={endDate:,pending:}
          db.collection(MYSTUDENT).updateOne({ "_id":ObjectId(mystudent._id)},{ $set: { endDate:new_date,pending:amountUp } }, (err, result) => {
            if(err){
              // res.json({ status: false, message: commonData.resMessages().msg8 });

            }else{
              // res.json({ status: true, message: commonData.resMessages().msg5 });
            }
          })
        }
      }
  
    }else{
    }

 
})
  console.log("running a task every minute");
});
// here is the magic
app.use(cors(corsOptions))
var db; 

mongodb.MongoClient.connect(config.dbString, { useNewUrlParser: true }, function (err, database) {
	assert.equal(null, err);

	db=database.db('PINTOFEES');
  require('./nextroute')(app, db);

	console.log('Database connected succesfully');

})
app.get('/', (req, res) => {
	res.send('Hello!  ' + PORT)
})
app.listen(PORT, (err) => {
	assert.equal(null, err);
	console.log('App started succesfully in port : ' + PORT);
})
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
