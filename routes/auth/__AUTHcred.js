
/* we need to require all the pages we create on "routes.js" file so that a link can be 
  established. If we do not need this the  request flow will be interrupted */

  module.exports =  (app,db)=>{
    const moment = require('moment');
    var ObjectId = require('mongodb').ObjectID;
    const dateformat = require('dateformat');
    const AUTH_SIGNUP = 'Pauth_Signup'; //expendibledetails=collection name
    const CATAGERY = 'PCategory';
    const MYSTUDENT = 'PStudent';
    var async = require("async")

    const commonData = require('../common/commonData');

    //signupAPI
    app.post('/signup_pinto', function (req, res, err) {
      var data=req.body;
     if((!data.name)&&(!data.email)&&(!data.password)&&(!data.og_name)&&(!data.phone)){
      res.json({ status: false, message: commonData.resMessages().msg1 });
    }else{
       var datas={
         "name":data.name,
         "email":data.email,
         "password":data.password,
         "organization":data.og_name,
         "phone":data.phone,
         "createddate":new Date(),
        //  "endDate":moment(new Date(), "DD-MM-YYYY").add(1, 'year'),
         "active":1
        }
        db.collection(AUTH_SIGNUP).find({ "phone": data.phone }).toArray().then(data => {
          if(data.length){
            res.json({ status: false, message: commonData.resMessages().msg6 });
          }else{
            db.collection(AUTH_SIGNUP).insert(datas, (err, result) => {
          if (err) res.json({ status: false, message: commonData.resMessages().msg2 });

          else res.json({ status: true, message: commonData.resMessages().msg5 });
      })
          }

       
      })
   
     }
        
     
    })
    //login
    app.post('/login_pinto', function (req, res, err) {
      var data=req.body;
      if((!data.phone)&&(!data.password)){
        res.json({ status: false, message: commonData.resMessages().msg1 });
      }else{ 
        db.collection(AUTH_SIGNUP).find({ "phone": data.phone,"password":data.password }).toArray().then(data => {
          if(data.length){
            res.json({ status: true, message: commonData.resMessages().msg5,data:data });
          }else{
             res.json({ status: false, message: commonData.resMessages().msg7 });

          }
        })

      }

    })
    //create category
    app.post('/create_category', function (req, res, err) {
      var data=req.body;
      console.log(req.body.category.replace(/ /g,'')
      ,"nidhuntta")
      if((!data.category)&&(!data.signupId)){
        res.json({ status: false, message: commonData.resMessages().msg1 });
      }else{ 
       db.collection(AUTH_SIGNUP).find({ "_id": ObjectId(data.signupId) }).toArray().then(data => {
          if(data.length){
            db.collection(CATAGERY).find({ "userId":req.body.signupId,"category":req.body.category.toUpperCase() }).toArray().then(data => {
              console.log(data,"nidhun1")
              if(data.length){
               res.json({ status: true, message: commonData.resMessages().msg10 });

              }else{
                  var category={
              "userId":req.body.signupId,
              "category":req.body.category.toUpperCase(),
              "createddate":new Date()

            }
            db.collection(CATAGERY).insert(category, (err, result) => {
              if (err) res.json({ status: false, message: commonData.resMessages().msg2 });
               else{
                res.json({ status: true, message: commonData.resMessages().msg5 });
              }
              })
              }
            })
        
          }else{
            res.json({ status: false, message: commonData.resMessages().msg2 });


          }
        })
      }
      


    })
    //get category
    app.post('/get_category', function (req, res, err) {
      console.log(req.body,"nidhun")
 if((!req.body.userId)){
        res.json({ status: false, message: commonData.resMessages().msg1 });
      }else{ 
        db.collection(CATAGERY).find({ "userId":req.body.userId}).toArray().then(data => {
          if(data.length){
            res.json({ status: true, message: commonData.resMessages().msg5,data:data });
          }else{
             res.json({ status: false, message: commonData.resMessages().msg8 });

          }
        })
      }
          })
          //delete
          app.post('/delete_category', function (req, res, err) {
            if((!req.body.deleteId)){
              res.json({ status: false, message: commonData.resMessages().msg1 });
            }else{ 
              db.collection(CATAGERY).find({ "_id":ObjectId(req.body.deleteId)}).toArray().then(data => {
                if(data.length){
                  db.collection(CATAGERY).remove({ "_id":ObjectId(req.body.deleteId)}, (err, result) => {
                    if(err){
                      res.json({ status: false, message: commonData.resMessages().msg8 });

                    }else{
                      res.json({ status: true, message: commonData.resMessages().msg5,data:data });
                    }
                  })
                  }else{
                   res.json({ status: false, message: commonData.resMessages().msg8 });
      
                }
              })
            }
                     })
                     app.post('/edit_category', function (req, res, err) {
                      console.log(req.body)
                      
                     if((!req.body.category)&&(!req.body.id)&&(!req.body.userId)){
                       res.json({ status: false, message: commonData.resMessages().msg1 });
                     }else{ 
                       db.collection(CATAGERY).find({ "_id":ObjectId(req.body.id),"userId":req.body.userId,"category":req.body.category.toUpperCase() }).toArray().then(data => {
                         if(data.length){
                          res.json({ status: false, message: commonData.resMessages().msg10 });

                           }else{
                            db.collection(CATAGERY).update({ "_id":ObjectId(req.body.id)},{ $set: { category: req.body.category.toUpperCase()  } }, (err, result) => {
                              if(err){
                                res.json({ status: false, message: commonData.resMessages().msg8 });
          
                              }else{
                                res.json({ status: true, message: commonData.resMessages().msg5 });
                              }
                            })
                         }
                       })
                     }
                              })
                     app.post('/add_student', function (req, res, err) {
                      var data=req.body;
                      var tdate=new Date();
                      var formatedDate =dateformat(tdate, 'dd-mm-yyyy');
                      if((!data.mobile)&&(!data.category)&&(!data.paymentmode)&&(!data.name)&&(!data.registrationFee)&&(!data.userId)){
                        res.json({ status: false, message: commonData.resMessages().msg1 });
                      }else{ 
                        db.collection(MYSTUDENT).find({ "phone": data.mobile,"userId": data.userId}).toArray().then(data => {
                          if(data.length){
                            console.log(data)
                            res.json({ status: false, message: commonData.resMessages().msg6 });
                          }else{
                      
                            if(req.body.paymentmode=="1month"){
                              var new_date = moment(tdate).add(1, 'month').format("DD-MM-YYYY");
                            }
                            if(req.body.paymentmode=="3month"){
                              var new_date = moment(tdate).add(3, 'month').format("DD-MM-YYYY");
                            }
                            if(req.body.paymentmode=="6month"){
                              var new_date = moment(tdate).add(6, 'month').format("DD-MM-YYYY");
                            }
                            if(req.body.paymentmode=="yearly"){
                              var new_date = moment(tdate).add(1, 'year').format("DD-MM-YYYY");
                            }
                            var dataNew={
                              "userId":req.body.userId,
                              "phone":req.body.mobile,
                              "registerdDate":formatedDate,
                              "category":req.body.category,
                              "endDate":new_date,
                              "paymentmode":req.body.paymentmode,
                              "name":req.body.name,
                              "registrationFee":parseInt(req.body.registrationFee),
                              "pending":0,
                              "amountPayed":0,
                              "activeFlag":1
                            }
                            console.log(dataNew,"1232y423448757834784")

  
                            db.collection(MYSTUDENT).insert(dataNew, (err, result) => {
                              if (err) res.json({ status: false, message: commonData.resMessages().msg2 });
                               else{
                                res.json({ status: true, message: commonData.resMessages().msg5 });
                              }
                              })
                          }
                
                       
                      })                     
                     }
                                                   
                    })
                    // view all contact
                    app.post('/all_student', function (req, res, err) {
                      var data=req.body;
                      if((!req.body.userId)){
                        res.json({ status: false, message: commonData.resMessages().msg1 });
                      }else{ 
                        if((req.body.userId)&&(!req.body.category)){
                          var query={"userId":req.body.userId}

                        }
                        if((req.body.userId)&&(req.body.category)){
                          var query={"userId":req.body.userId,"category":req.body.category}

                        }
                        db.collection(MYSTUDENT).find(query).toArray().then(data => {
                          if(data.length){
                            console.log(data,"data here")
                            res.json({ status: true, message: commonData.resMessages().msg5,data:data });
                          }else{
                            res.json({ status: false, message: commonData.resMessages().msg8 });

                          }
                        })
                      }
                    })
   //today_activity  
   app.post('/today_activity',  function  (req, res, err) {
    var data=req.body;
    if((!req.body.userId)&&(!req.body.activity)){
      res.json({ status: false, message: commonData.resMessages().msg1 });
    }else{ 
      if(data.activity=="today"){
        var tdate=new Date();

        var formatedDate =dateformat(tdate, 'dd-mm-yyyy');
        var time= { pending: { $gt: 0 } ,"userId":req.body.userId} 
        db.collection(MYSTUDENT).find(time).toArray().then(data => {
          if(data.length>0){
          console.log(data,"data here")
          res.json({ status: true, message: commonData.resMessages().msg5 ,data:data});
  
            }else{
              res.json({ status: false, message: commonData.resMessages().msg8 });
            }
  
       
      }) 
      }
  
      // if(data.activity=="thisweek"){
      //   var week=new Date();
      //   var weekend =week.setDate(week.getDate() + 7);
      //   var testme =dateformat(weekend, 'dd-mm-yyyy');
      //   var tdate=new Date();
      //   var formatedDate =dateformat(tdate, 'dd-mm-yyyy');
      //   var time= { endDate: {  $gte:formatedDate, $lt: testme } ,"userId":req.body.userId} 
      //   console.log(time,"time da")
      //   db.collection(MYSTUDENT).find(time).toArray().then(data => {
      //     if(data.length){
      //       console.log(data,"data here12344")
      //     res.json({ status: true, message: commonData.resMessages().msg5 ,data:data});
  
      //       }else{
      //         res.json({ status: false, message: commonData.resMessages().msg8 });
      //       }
  
      //   }) 
      // }
  
    }
  }) 
  //
           //delete student
           app.post('/delete_student', function (req, res, err) {
             console.log(req.body,"delete student")
            if((!req.body.deleteId)){
              res.json({ status: false, message: commonData.resMessages().msg1 });
            }else{ 
              db.collection(MYSTUDENT).find({ "_id":ObjectId(req.body.deleteId)}).toArray().then(data => {
                if(data.length){
                  db.collection(MYSTUDENT).remove({ "_id":ObjectId(req.body.deleteId)}, (err, result) => {
                    if(err){
                      res.json({ status: false, message: commonData.resMessages().msg8 });

                    }else{
                      res.json({ status: true, message: commonData.resMessages().msg5,data:data });
                    }
                  })
                  }else{
                   res.json({ status: false, message: commonData.resMessages().msg8 });
      
                }
              })
            }
                     })  
                     //do payment
                     app.post('/do_payment', function (req, res, err) {
                       if((!req.body._id)&&(req.body.userId)&(req.body.mode)&&(req.body.amount)){
                        res.json({ status: false, message: commonData.resMessages().msg1 });
                      }else{ 
                        var toSearch={_id:ObjectId(req.body._id),userId:req.body.userId}
                        db.collection(MYSTUDENT).find(toSearch).toArray().then(data => {
                          if(data.length>0){
                            console.log(data,"data here")
                            if(req.body.mode=="Pending"){
                              db.collection(MYSTUDENT).update({ _id:ObjectId(req.body._id),userId:req.body.userId},{ $set: { "pending":parseInt(req.body.amount),"remark":req.body.remark,"modifyDate":new Date()}}, (err, result) => {
                                if(err){
                                  res.json({ status: false, message: commonData.resMessages().msg2 });
                                }else{
                                  console.log(result,"5875478935789")
                                  res.json({ status: true, message: commonData.resMessages().msg5 });
                                }
                              })
                                                
                             }
                             if(req.body.mode=="Amount"){
                              db.collection(MYSTUDENT).update({ _id:ObjectId(req.body._id),userId:req.body.userId},{ $set: { "amountPayed":parseInt(req.body.amount),"remark":req.body.remark,"modifyDate":new Date()}}, (err, result) => {
                                if(err){
                                  res.json({ status: false, message: commonData.resMessages().msg2 });
                                }else{
                                  res.json({ status: true, message: commonData.resMessages().msg5 });
                                }
                              })
                                                
                             }
                           
                        
                          }else{
                            res.json({ status: false, message: commonData.resMessages().msg8 });
                          }
                        })
                      }
                     })    
                     //get_updated_payment 
                     app.post('/get_updated_payment', function (req, res, err) {
                      if((!req.body._id)&&(req.body.userId)){
                        res.json({ status: false, message: commonData.resMessages().msg1 });
                      }else{ 
                        var toSearch={_id:ObjectId(req.body._id),userId:req.body.userId}
                        db.collection(MYSTUDENT).find(toSearch).toArray().then(data => {
                          if(data.length>=0){
                            res.json({ status: true, message: commonData.resMessages().msg5,data:data });
                          }else{

                            res.json({ status: false, message: commonData.resMessages().msg8 });
                          }
                        })
                        }
                                           
                    })       
}

